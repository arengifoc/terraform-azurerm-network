data "azurerm_resource_group" "selected" {
  name = var.resource_group
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  resource_group_name = var.resource_group
  location            = data.azurerm_resource_group.selected.location
  address_space       = [var.vnet_cidr]
  dns_servers         = var.dns_servers
  tags                = var.tags
}

resource "azurerm_subnet" "subnet" {
  count                = length(var.subnet_names)
  name                 = var.subnet_names[count.index]
  resource_group_name  = var.resource_group
  address_prefixes     = [var.subnet_cidrs[count.index]]
  virtual_network_name = azurerm_virtual_network.vnet.name
}
