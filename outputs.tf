output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = azurerm_virtual_network.vnet.id
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = azurerm_virtual_network.vnet.name
}

output "vnet_location" {
  description = "The location of the newly created vNet"
  value       = azurerm_virtual_network.vnet.location
}

output "vnet_address_space" {
  description = "The address space of the newly created vNet"
  value       = azurerm_virtual_network.vnet.address_space
}

output "subnet_ids" {
  description = "The IDs of subnets created inside the new vNet"
  value       = azurerm_subnet.subnet.*.id
}

output "subnet_names" {
  description = "The names of subnets created inside the new vNet"
  value       = [for item in azurerm_subnet.subnet.*.id: split("/", item)[10]]
}
